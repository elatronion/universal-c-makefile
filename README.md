# Universal C Makefile

This makefile should allow it's users to compile C code to Linux or Windows from Linux.

You may compile for *Linux* or *Windows*, *32* or *64* bit, **easily**!
Both *executables* and *libraries*, *static* or *dynamic*.

## Usage

### Project Structure

This makefile respects a specific file structure to help ease organisation of compilation.

```
.
├── build
│   └── <OS>
│       └── <CPU>
│           ├── output.a / output.lib
│           ├── output.out / output.exe
│           ├── output.so / output.dll
│           └── src
│               ├── *.c.d
│               └── *.c.o
├── inc
│   └── *.h
├── lib
│   └── <OS>
│       └── <CPU>
│           └── *.a / *.lib
├── makefile
└── src
    ├── *.c
    └── *.h
```

Basically:
-  Place all your source files in **./src/**
-  Place all your includes in **./include/**
-  Place all your libraries in **./lib/[OS]/[CPU]/**

Once you make your project, all compiled files will end up in **./build/[OS]/**

### Preparing the makefile

Before compiling your project, you could set a couple of flags if you plan on using libraries or generating debug information.

### Making Executables

```bash
make TARGET_OS=<linux/windows> TARGET_CPU=<i686/x86_64>
```

### Making Libraries

```bash
make TARGET_OS=<linux/windows> TARGET_CPU=<i686/x86_64> ARCHIVE=<true/false> ARCHIVE_STATIC=<true/false>
```
